#!/usr/bin/env bash
BASEDIR=$(dirname $0)

if [[ ! -f $BASEDIR/version_parameters ]]; then
	echo Missing version_parameters in $BASEDIR
	echo The file should include these lines \(change value as needed\):
	echo version_pattern="(?<=VERSION \= \")([0-9.]+)(?=\")"
	echo check_files=cloudinary-core/src/main/java/com/cloudinary/Cloudinary.java
	echo change_files="gradle.properties README.md cloudinary-core/src/main/java/com/cloudinary/Cloudinary.java"
	exit 0
fi

new_version=$1
. $BASEDIR/version_parameters
current_version=`grep -oP "$version_pattern" $check_files`
current_version_re=${current_version//./\\.}
echo $current_version
if [ -n "$new_version" ]; then
    echo "New version will be $new_version"
    sed -e "s/${current_version_re}/${new_version}/g" -i "" $change_files
    git changelog -t $new_version
else
    echo "Usage: $0 <new version>"
    echo "For example: $0 1.9.2"
fi
