#!/usr/bin/env node

"use strict";
let cloudinary = require('../cloudinary');
let Q = require('q');

function wipeUploadPresets() {
  const api = cloudinary.v2.api;
  api.upload_presets({max_results: 100}, (error, response)=>{
    let deleteRequests = [];
    if(error) {
      console.error("failed to list upload presets", error);
      
    } else {
      response.presets.forEach( (p)=>{
        let keep = ['angular-sample', 'cloudinary_java_test', 'ios_unsigned_upload', 'swift-example'];
        if(keep.indexOf(p.name) === -1) {
          console.log("deleting upload preset ", p.name);
          deleteRequests.push(api.delete_upload_preset(p.name));
        }
      });
      Q.allSettled(deleteRequests).then(
        (results)=> console.log("all deleted", results),
        (errors)=> console.log(errors));
      
    }
  })
}
if( typeof module !== 'undefined'){
  module.exports = wipeUploadPresets;
} else {
  wipeUploadPresets();
}