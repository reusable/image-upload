let cloudinary = require('../cloudinary').v2;
let allCursors = require('./all_cursors');

cloudinary.config(true);

let deleteAllResources = allCursors(cloudinary.api.delete_all_resources, results=>Object.entries(results.deleted));
module.exports = deleteAllResources;

