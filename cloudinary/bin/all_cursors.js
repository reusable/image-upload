"use strict";
const Q = require('q');
const isFunction = require('lodash/isFunction');

/**
 * Recursively calls the `pagedApi` to get all results.
 * Each iteration receives the `NEXT_CURSOR` from the previous call.
 * @param pagedApi
 * @param {collector} collect extract and process the list from the result body
 * @returns {AggrApi} the new aggregating function
 * @example
 * var cloudinary = require('../cloudinary').v2;
 *
 * var r;
 * var listAllResources = allCursors(cloudinary.api.resources, (result)=> result.resources);
 *
 * listAllResources((resources)=> {console.log("got " + resources.length + " resources");r = resources;});
 *
 * var listAllTransformations= allCursors(cloudinary.api.transformations, (result)=> result.transformations);
 * listAllTransformations(function (transformations) {
 *   console.log("got " + transformations.length + " transformations");
 *   r = transformations;
 * });
 *
 */
(function(){
var counter = 0;

function allCursors(pagedApi, collect) {
  /**
   * @function AggrApi
   * @param options
   * @param callback
   * @returns {Promise}
   */
  counter++;
  let aggrApi = function (options, callback, level =0) {
    var combined = []
    // console.count("aggrApi" + counter);
    return Q.Promise((resolve, reject) => {
      console.log(`Start promise level ${level}`);
      if (!callback) {
        if (isFunction(options)) {
          callback = options;
          options = {};
        } else {
          callback = (e, r) => console.log("default callback");
        }
      }
      if (options && options.hasOwnProperty("next_cursor") && options.next_cursor === undefined) {
        reject({message: "next_cursor provided as undefined"});
        callback({message: "next_cursor provided as undefined"}, []);
      }
      pagedApi(
        options
      ).then(function (result) {
        console.log(`pagedApi then ${level} combined ${combined}`);
        let collected = collect(result);
        combined = combined.concat(result);
        if (!result.next_cursor) { // end of the chain
          // console.log("end of chain", result);
          resolve(collected);
          callback(undefined, collected);
        } else {
          return aggrApi(
            Object.assign({max_results: 500}, options, {next_cursor: result.next_cursor}), null, level +1
          ).catch((error) => {
            if(error.message) {
              console.log(`level ${level}, error is ${JSON.stringify(error)}` )
            } else {
              console.log(`***************level ${level}, error does not has itself`)
              
            }
            let err = {soFar: combined, ...error};
            // console.log(`aggr catch ${level}`);
            reject(err);
            callback(err)
          }).then(result => {
            // combined = combined.concat(result);
            console.log( `inner pagedApi then ${level} ${combined} ${result}`);
            resolve(result);
            callback(undefined, result);
          })

        }
      }).catch(function (error) {
        console.error(`catch pagedApi ${level} combined ${combined}`);
        let err = {...error, combined};
        reject(err);
        callback(err);
      }).done();
    });
  };
  return aggrApi;
}


/**
 * @callback collector
 * @param {Object} result the result of the api call
 * @return {array} the list of items extracted from the result
 * @type {allCursors}
 */


/*

// Code Examples

var cloudinary = require('../cloudinary').v2;

var r;
var listAllResources = allCursors(cloudinary.api.resources, (result)=> result.resources);

listAllResources((resources)=> {console.log("got " + resources.length + " resources");r = resources;});

var listAllTransformations= allCursors(cloudinary.api.transformations, (result)=> result.transformations);
listAllTransformations(function (transformations) {
  console.log("got " + transformations.length + " transformations");
  r = transformations;
});

*/
allCursors.test = function () {
  let testFunc = function (x, callback) {
    if (!callback) {
      if (!x) {
        x = {};
      }
      if (isFunction(x)) {
        callback = x;
        x = {};
      } else {
        callback = (e, r) => 0;
      }
    }
    if (x.next_cursor > 20) {return}
    return Q.Promise(function (resolve, reject) {
      let nextcursor = x.next_cursor || 0;
      let r = {...x, value: [nextcursor + 1]};
      let shouldStop = x.simulateError && x.simulateError(r);

      if (shouldStop) {
        console.log("testFunc Stopping");
        let err = {message: "invalid cursor " + shouldStop};
        reject(err);
        callback(err, );
      }

      if (nextcursor < 10) {
        r.next_cursor = nextcursor + 1;
      } else {
        delete r.next_cursor;
      }

      resolve(r);
      callback(undefined, r);

    });
  };
  // console.log("test test");
  // testFunc()
  let b = allCursors(testFunc, (result) => result.value)
  // console.log("=========callback");
  // b({}, (e, r) => e ? console.log("callback error", e) : console.log("callback success:", r));
  // console.log("=========promise")
  // b({}).then((r) => console.log("promise success:", r)).catch(e => console.error("promise error", e))
  //
  // console.log("=========stopped callback");
  // b({simulateError: r => (r.next_cursor === 9 && "reached 10")},
  //   (e, r) => console.log(e,r) && (e ? console.log("stopped callback error", e) : console.log("stopped callback success:", r))).done();
  console.log("=========promise")
  b({simulateError: r => (r.next_cursor === 9 && "reached 10")}).then((r) => console.log("stopped promise success:",
    r)).catch(e => console.error("stopped promise error", e)).done();
  console.log("=========promise exception")
  b({simulateError: r => {if (r.next_cursor > 3) {console.log("throwing"); throw new Error("reached 10")}}}).then((r) => console.log(
    "stopped promise exception success:",
    r)).catch(e => console.error("stopped promise exception error", e))
}


module.exports = allCursors;
})()