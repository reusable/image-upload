(function() {
  var ClientRequest, Q, api_http, cloneDeep, config, expect, http, https, isEmpty, isFunction, last, querystring, ref, sharedExamples, sinon, utils,
    slice = [].slice;

  expect = require('expect.js');

  utils = require("../lib/utils");

  isEmpty = utils.isEmpty, isFunction = utils.isFunction, last = utils.last;

  isFunction = require('lodash/isFunction');

  cloneDeep = require('lodash/cloneDeep');

  config = require("../lib/config");

  http = require('http');

  https = require('https');

  if (config().upload_prefix && config().upload_prefix.slice(0, 5) === 'http:') {
    api_http = http;
  } else {
    api_http = https;
  }

  querystring = require('querystring');

  sinon = require('sinon');

  ClientRequest = require('_http_client').ClientRequest;

  Q = require('q');

  exports.TIMEOUT_SHORT = 5000;

  exports.TIMEOUT_MEDIUM = 20000;

  exports.TIMEOUT_LONG = 50000;

  exports.SUFFIX = (ref = process.env.TRAVIS_JOB_ID) != null ? ref : Math.floor(Math.random() * 999999);

  exports.SDK_TAG = "SDK_TEST";

  exports.TEST_TAG_PREFIX = "cloudinary_npm_test";

  exports.TEST_TAG = exports.TEST_TAG_PREFIX + "_" + exports.SUFFIX;

  exports.UPLOAD_TAGS = [exports.TEST_TAG, exports.TEST_TAG_PREFIX, exports.SDK_TAG];

  exports.IMAGE_FILE = "test/resources/logo.png";

  exports.LARGE_RAW_FILE = "test/resources/TheCompleteWorksOfShakespeare.mobi";

  exports.LARGE_VIDEO = "test/resources/CloudBookStudy-HD.mp4";

  exports.EMPTY_IMAGE = "test/resources/empty.gif";

  exports.RAW_FILE = "test/resources/docx.docx";

  exports.ICON_FILE = "test/resources/favicon.ico";

  exports.IMAGE_URL = "http://res.cloudinary.com/demo/image/upload/sample";

  exports.test_cloudinary_url = function(public_id, options, expected_url, expected_options) {
    var url;
    url = utils.url(public_id, options);
    expect(url).to.eql(expected_url);
    expect(options).to.eql(expected_options);
    return url;
  };

  expect.Assertion.prototype.produceUrl = function(url) {
    var actual, actualOptions, options, public_id, ref1;
    ref1 = this.obj, public_id = ref1[0], options = ref1[1];
    actualOptions = cloneDeep(options);
    actual = utils.url(public_id, actualOptions);
    this.assert(actual.match(url), function() {
      return "expected '" + public_id + "' and " + (JSON.stringify(options)) + " to produce '" + url + "' but got '" + actual + "'";
    }, function() {
      return "expected '" + public_id + "' and " + (JSON.stringify(options)) + " not to produce '" + url + "' but got '" + actual + "'";
    });
    return this;
  };

  expect.Assertion.prototype.emptyOptions = function() {
    var actual, options, public_id, ref1;
    ref1 = this.obj, public_id = ref1[0], options = ref1[1];
    actual = cloneDeep(options);
    utils.url(public_id, actual);
    this.assert(isEmpty(actual), function() {
      return "expected '" + public_id + "' and " + (JSON.stringify(options)) + " to produce empty options but got " + (JSON.stringify(actual));
    }, function() {
      return "expected '" + public_id + "' and " + (JSON.stringify(options)) + " not to produce empty options";
    });
    return this;
  };

  expect.Assertion.prototype.beServedByCloudinary = function(done) {
    var actual, actualOptions, callHttp, options, public_id, ref1;
    ref1 = this.obj, public_id = ref1[0], options = ref1[1];
    actualOptions = cloneDeep(options);
    actual = utils.url(public_id, actualOptions);
    if (actual.startsWith("https")) {
      callHttp = https;
    } else {
      callHttp = http;
    }
    callHttp.get(actual, (function(_this) {
      return function(res) {
        _this.assert(res.statusCode === 200, function() {
          return "Expected to get " + actual + " but server responded with \"" + res.statusCode + ": " + res.headers['x-cld-error'] + "\"";
        }, function() {
          return "Expeted not to get " + actual + ".";
        });
        return done();
      };
    })(this));
    return this;
  };

  sharedExamples = (function() {
    function sharedExamples(name, examples) {
      if (this.allExamples == null) {
        this.allExamples = {};
      }
      if (isFunction(examples)) {
        this.allExamples[name] = examples;
        examples;
      } else {
        if (this.allExamples[name] != null) {
          return this.allExamples[name];
        } else {
          return function() {
            return console.log("Shared example " + name + " was not found!");
          };
        }
      }
    }

    return sharedExamples;

  })();

  exports.sharedExamples = exports.sharedContext = sharedExamples;

  exports.itBehavesLike = function() {
    var args, name;
    name = arguments[0], args = 2 <= arguments.length ? slice.call(arguments, 1) : [];
    return context("behaves like " + name, function() {
      return sharedExamples(name).apply(this, args);
    });
  };

  exports.includeContext = function() {
    var args, name;
    name = arguments[0], args = 2 <= arguments.length ? slice.call(arguments, 1) : [];
    return sharedExamples(name).apply(this, args);
  };


  /**
    Create a matcher method for upload parameters
    @private
    @function helper.paramMatcher
    @param {string} name the parameter name
    @param value {Any} the parameter value
    @return {(arg)->Boolean} the matcher function
   */

  exports.uploadParamMatcher = function(name, value) {
    return function(arg) {
      var EncodeFieldPart, return_part;
      EncodeFieldPart = function(name, value) {};
      return_part = 'Content-Disposition: form-data; name="' + name + '"\r\n\r\n';
      return_part += value;
      return new RegExp(return_part).test(arg);
    };
  };


  /**
    Create a matcher method for api parameters
    @private
    @function helper.apiParamMatcher
    @param {string} name the parameter name
    @param value {Any} the parameter value
    @return {(arg)->Boolean} the matcher function
   */

  exports.apiParamMatcher = function(name, value) {
    var expected, params;
    params = {};
    params[name] = value;
    expected = querystring.stringify(params);
    return function(arg) {
      return new RegExp(expected).test(arg);
    };
  };


  /**
    Escape RegExp characters
    @private
    @param {string} s the string to escape
    @return a new escaped string
   */

  exports.escapeRegexp = function(s) {
    return s.replace(/[{\[\].*+()}]/g, (function(_this) {
      return function(c) {
        return '\\' + c;
      };
    })(this));
  };


  /**
    @function mockTest
    @nodoc
    Provides a wrapper for mocked tests. Must be called in a `describe` context.
    @example
    <pre>
    const mockTest = require('./spechelper').mockTest
    describe("some topic", function() {
      mocked = mockTest()
      it("should do something" function() {
        options.access_control = [acl];
        cloudinary.v2.api.update("id", options);
        sinon.assert.calledWith(mocked.writeSpy, sinon.match(function(arg) {
          return helper.apiParamMatcher('access_control', "[" + acl_string + "]")(arg);
      })
    );
    </pre>
    @return {object} the mocked objects: `xhr`, `write`, `request`
   */

  exports.mockTest = function() {
    var mocked;
    mocked = {};
    before(function() {
      mocked.xhr = sinon.useFakeXMLHttpRequest();
      mocked.write = sinon.spy(ClientRequest.prototype, 'write');
      return mocked.request = sinon.spy(api_http, 'request');
    });
    after(function() {
      mocked.request.restore();
      mocked.write.restore();
      return mocked.xhr.restore();
    });
    return mocked;
  };


  /**
    @callback mockBlock
    A test block
    @param xhr 
    @param writeSpy 
    @param requestSpy 
    @return {*} a promise or a value
   */


  /**
    @function mockPromise
    Wraps the test to be mocked using a promise.
    Can be called inside `it` functions
    @param {mockBlock} the test function, accepting (xhr, write, request)
    @return {Promise}
   */

  exports.mockPromise = function(mockBlock) {
    var requestSpy, writeSpy, xhr;
    xhr = void 0;
    writeSpy = void 0;
    requestSpy = void 0;
    return Q.Promise(function(resolve, reject, notify) {
      var mock, result;
      xhr = sinon.useFakeXMLHttpRequest();
      writeSpy = sinon.spy(ClientRequest.prototype, 'write');
      requestSpy = sinon.spy(api_http, 'request');
      mock = {
        xhr: xhr,
        writeSpy: writeSpy,
        requestSpy: requestSpy
      };
      result = mockBlock(xhr, writeSpy, requestSpy);
      if (isFunction(result != null ? result.then : void 0)) {
        return result.then(resolve);
      } else {
        return resolve(result);
      }
    })["finally"](function() {
      requestSpy.restore();
      writeSpy.restore();
      return xhr.restore();
    });
  };

}).call(this);

//# sourceMappingURL=spechelper.js.map
