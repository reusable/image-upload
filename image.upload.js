module.exports = function (ngModule) {
    ngModule.service('imageUploader', [function () {
        var uploader = this

        var cloudinary = require('./cloudinary')

        var notification_url;

        uploader.initialize = function (cloud_name, api_key, api_secret, notification_url_from_app) {
            console.log("initiliaze with ", cloud_name, api_key, api_secret, notification_url_from_app)
            cloudinary.config({
                cloud_name: cloud_name,
                api_key: api_key,
                api_secret: api_secret
            });
            notification_url = notification_url_from_app
        }

        uploader.upload = function (base64Data, image_public_id) {
            console.log('notification url is ', notification_url)
            cloudinary.v2.uploader.upload(base64Data,
                {
                    notification_url: notification_url,
                    public_id: image_public_id
                },
                function (err, result) {
                    console.log(err, result)
                })
        }
    }])
}
